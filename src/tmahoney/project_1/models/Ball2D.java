package tmahoney.project_1.models;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Stroke;

public class Ball2D
{
	private static final Color STROKE_COLOR = new Color(0, 0, 0, 20);
	private static final Stroke STROKE = new BasicStroke(10);
	
	private Vector2D center;
	private Color color;
	private float radius;

	/**
	 * @param center the new center for this ball.
	 */
	public void setCenter(Vector2D center)
	{
		this.center = center;
	}

	/**
	 * @param color the new color of this ball.
	 */
	public void setColor(Color color)
	{
		this.color = color;
	}

	/**
	 * @param radius the new radius of this ball.
	 */
	public void setRadius(float radius)
	{
		this.radius = radius;
	}
	
	/**
	 * @return the radius of this ball.
	 */
	public float getRadius()
	{
		return radius;
	}
	
	/**
	 * @return the center of this ball.
	 */
	public Vector2D getCenter()
	{
		return center;
	}
	
	/**
	 * @return the color of this ball.
	 */
	public Color getColor()
	{
		return color;
	}

	public void paint(Graphics g)
	{
		((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.setColor(color);
		int size = (int) (radius * 2);
		g.fillOval((int) (center.x - radius), (int) (center.y - radius), size,
				size);
		g.setColor(STROKE_COLOR);
		((Graphics2D) g).setStroke(STROKE);
		g.drawOval((int) (center.x - radius), (int) (center.y - radius), size,
				size);
	}
}
