package tmahoney.project_1.demos;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Random;

import tmahoney.project_1.models.Ball2D;
import tmahoney.project_1.models.Vector2D;

public class OrderedPaint
{
	private static final int WIDTH = 1000;
	private static final int HEIGHT = 600;
	private static final int MAX_BALLS = 1000;
	
	private static Ball2D[] balls;
	private static float[] ballRadii;
	private static int numberOfBalls;
	private static Frame frame;
	
	public static void main(String... args)
	{
//		Scanner scanner = new Scanner(System.in);
//		System.out.print("Please enter the number of balls you wish to draw: ");
//		numberOfBalls = scanner.nextInt();
//		System.out.println("Thanks!");		
		numberOfBalls = 20;
		if (numberOfBalls > MAX_BALLS)
		{
			numberOfBalls = MAX_BALLS;
			System.out.println("You entered a number greater than the maximum (" + MAX_BALLS + ").");
			System.out.println("This will now display " + MAX_BALLS + " balls.");
		}
		
		// Create the balls and order them.
		Color pink = new Color(240, 0, 140);
		Color green = new Color(223, 242, 38);
		Color blue = new Color(45, 217, 231);
		Color lightGrey = new Color(100, 100, 100);
		Color darkGrey = new Color(30, 30, 30);
		balls = new Ball2D[numberOfBalls];
		ballRadii = new float[numberOfBalls];
		Random random = new Random();
		for (int i = 0; i < numberOfBalls; i++)
		{
			Ball2D ball = new Ball2D();
			Vector2D center = new Vector2D();
			center.x = random.nextFloat() * WIDTH;
			center.y = random.nextFloat() * HEIGHT;
			ball.setCenter(center);
			ball.setRadius(random.nextFloat() * ((WIDTH + HEIGHT) / 8));
			float colorRandom = random.nextFloat();
			if (colorRandom < 0.13)
				ball.setColor(blue);
			else if (colorRandom < 0.26)
				ball.setColor(pink);
			else if (colorRandom < 0.45)
				ball.setColor(green);
			else if (colorRandom < 0.7)
				ball.setColor(lightGrey);
			else
				ball.setColor(darkGrey);
			
			balls[i] = ball;
			ballRadii[i] = ball.getRadius();
		}
		
		Arrays.sort(balls, new Comparator<Ball2D>()
		{
			@Override
			public int compare(Ball2D o1, Ball2D o2)
			{
				return (int) (o2.getRadius() - o1.getRadius());
			}
		});
		
		// Create the ball drawing canvas.
		final Canvas canvas = new Canvas()
		{
			private static final long serialVersionUID = 1L;

			public void paint(Graphics g)
			{
				for (Ball2D ball : balls)
					ball.paint(g);
			}
		};
		canvas.setPreferredSize(new Dimension(WIDTH, HEIGHT));
		
		// Show the canvas in a frame.
		frame = new Frame();
		frame.setBackground(new Color(36, 36, 36));
		frame.setPreferredSize(new Dimension(WIDTH, HEIGHT + frame.getInsets().top));
		frame.add(canvas);
		frame.pack();
		frame.setVisible(true);
	}
}
